`
  JS is not functional language
  It gives very wide opportunities to apply specific paradigm
`


const positive1 = "Easy to read, declarative code";
const positive2 = "Easier debugging";
const positive3 = "Performance boost due to immutability";
const positive4 = "Happy developers";

const negative1 = "Possible inefficiencies";
const negative2 = "Readability can also suffer";





`Functional style primitives`

const foo = {};

function bar() {}



















`First class function`

const bar2 = function() {};
const bar3 = () => {};













`Higher order function`
function fistClass(callback) {
  callback();
}

function firstClassAgain() {
  return () => "Hello world";
}














`
Closures
  - privacy
  - encapsulation
`
function makeFunc() {
  var name = "Mozilla";
  function displayName() {
    alert(name);
  }
  return displayName;
}

function closure() {
  // privacy + state container
  const abc = {};

  return {
    hello() {
      return "world";
    },
  };
}
















`Arity`
const nullary = () => "foo";
const unary = x => x;
const binary = (x, y) => x + y;

const unaryObject = ({ x, y }) => x + y;














`pointed vs point-free`
const pointed = x => x.map().filter().reduce().toString();

const pointFree = R.pipe(R.sortBy(R.prop("age")), R.last, R.prop("name"));















`
currying
f(a,b,c) -> f(a)(b)(c)

partial application
f(a,b,c) -> f(a)(b,c)
`

const currying = x => y => z => x + y + z;

const partialApply = (x,y) => z => x + y + z;



















`composition
 pipe/compose`

const pipe = (...fns) => val => fns.reduce((acc, fn) => fn(acc), val);
const add1 = x => x + 1;
const toString = x => x.toString();
const floor = x => `__${x}__`;

const doUselessPipe = pipe(add1, toString, floor);
doUselessPipe(6);














`
pure function + stateless
doesn't have side effect
always returns the same results for the given arguments
`
const pure = x => x + 5;

let i = 0;
const notPure = x => {
  i = i +1;
  return x + i;
}
 







`Immutability`

function mutable(arr) {
  arr.forEach(elem => {
    elem + 1;
  });

  return arr;
}

function immutable(arr) {
  return arr.map(x => x + 1);
}

`
non mutating
- Array.prototype.map()
- Array.prototype.filter()
- Array.prototype.reduce()
- Array.prototype.flat()
- Array.prototype.flatMap()
- Array.prototype.concat()
- Array.prototype.slice()
- Array.prototype.reduceRight()

mutating
- Array.prototype.push()
- Array.prototype.sort()
- Array.prototype.reverse()
- Array.prototype.forEach()
- Array.prototype.pop()
- Array.prototype.shift()
- Array.prototype.splice()
- Array.prototype.unshift()
`











`Memoization`
const memoizedSelector = (x, y) => /* do something expensive*/ `${x}+${y}`;















`Rest/spread FTW`
const a = [1, 4, 5];
const b = [5, 3, 1];
const c = [...a, ...b];

const [d, ...e] = c;

const obj = {
  a: 1,
  b: 2,
};

const { a, ...rest } = obj;














`How far we can go?
 - pureness
 - effects isolation
 - Either
`
















`
Ramda.js (lens, transducer)
https://ramdajs.com/


Functional-Light JavaScript
https://github.com/getify/Functional-Light-JS

Mostly Adequate Guide
https://github.com/MostlyAdequate/mostly-adequate-guide

FP-TS
https://github.com/gcanti/fp-ts

Awesome functional javascript
https://github.com/stoeffel/awesome-fp-js

`
